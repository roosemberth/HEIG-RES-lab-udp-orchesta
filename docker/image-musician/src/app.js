const instrumentSounds = {
  piano: "ti-ta-ti",
  trumpet: "pouet",
  flute: "trulu",
  violin: "gzi-gzi",
  drum: "boum-boum",
}

function printHelp() {
  const instrumentNames = Object.keys(instrumentSounds);
  console.log([
    `Usage: docker run <image_name> <instrument>`,
    "",
    "Where instrument is one of the following:",
    instrumentNames.map((s) => `  ${s}`),
  ].join("\n"));
}


const udp = require('dgram');
const client = udp.createSocket('udp4');
var message = new Buffer("");
var musicianUuid = 0;
function sendSound()
{
  client.send(message, 0, message.length, 27000, '172.17.0.255', function(error){
    if(error){
      client.close();
    }else{
      console.log('Sound send');
    }
  });
}

function playMusic(instrumentName) {
  console.log([
    "I will play with the",
    instrumentName,
    "with sound",
    `"${instrumentSounds[instrumentName]}"`,
    "."
  ].join(" "));
  message = new Buffer(musicianUuid + " " + instrumentSounds[instrumentName]);
  setInterval(sendSound,1000)
}

function main() {
  const cmdline = process.argv.slice(2);
  if (cmdline.length < 1) {
    console.error([
      "Please specify the instrument I shall be playing.",
      "Use --help to list the instruments I know how to play.",
    ].join("\n"));
    process.exit(1);
  }

  if (cmdline[0] === "--help") {
    printHelp();
    process.exit(0);
  }

  const instrument = cmdline[0];
  if (!(instrument in instrumentSounds)) {
    console.error([
      "I don't know how to play the specified instrument: " + instrument,
      "Use --help to list the instruments I know how to play."
    ].join("\n"));
    process.exit(1);
  }
  
  //Ne marche pas
  //const uuid = require('uuidv4');
  //musicianUuid = uuid.uuid();
  musicianUuid=0;
  console.log("Musician uuid : "+ musicianUuid);
  
  playMusic(instrument);
}

main();

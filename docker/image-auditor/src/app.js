const dgram=require('dgram');
const net = require('net');
const udpServer = dgram.createSocket('udp4');
const tcpServer = net.Server();

const udpPort = 27000;
const tcpPort = 2205;

var map = new Map(); // map of key being UUID and value being a tuple of instrument and date object for last-heard time


udpServer.on('message', (msg, info) => {
  console.log(`Server got: ${msg} from ${info.address}:${info.port}`);
  var splitMessage = msg.toString().split(" ");
  var clientUuid = splitMessage[0];
  var clientInstrument = splitMessage[1];
  var date = new Date();
  var tuple = [match(clientInstrument), date];
  map.set(clientUuid, tuple);
})

udpServer.on('error', (err) => {
  console.log(err.stack);
  server.close();
})


tcpServer.on('connection', function(socket) {
  //Pour tester, a supprimer ensuite
  var date = new Date();
  var tuple = ["piano", date];
  map.set("0ab", tuple);
	
  console.log('A new connection has been established.');
  socket.on('data', function(chunk) {
      console.log(`Data received from client: ${chunk.toString()}.`);
  });
  
  socket.on('end', function() {
      console.log('Closing connection with the client');
  });
  
  socket.on('error', function(err) {
      console.log(`Error: ${err}`);
  });
  
  var resultat = [];
  
  map.forEach(function logMapElements(value, key, map) {
	if((new Date()) - value[1] < 5000)
	{
      resultat.push({uuid:key, instrument:value[0], activeSince:value[1]});
	}
  });
  
  socket.write(JSON.stringify(resultat));
  
  socket.end();
});

const instrumentSounds = {
  piano: "ti-ta-ti",
  trumpet: "pouet",
  flute: "trulu",
  violin: "gzi-gzi",
  drum: "boum-boum",
}

function main() {
  console.log("Hello world!");
  udpServer.bind(udpPort);
  tcpServer.listen(tcpPort, function() {
    console.log(`Server listening for connection requests on socket localhost:${tcpPort}.`);
  });
}

function match(clientInstrument) {
  switch(clientInstrument) {
    case instrumentSounds.drum:
      return "drum";
    case instrumentSounds.flute:
      return "flute";
    case instrumentSounds.piano:
      return "piano";
    case instrumentSounds.trumpet:
      return "trumpet";
    case instrumentSounds.violin:
      return "violin";
  }
}

main();
